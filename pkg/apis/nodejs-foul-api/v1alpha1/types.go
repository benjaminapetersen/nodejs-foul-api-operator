package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type NodeJSFoulAPIList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []NodeJSFoulAPI `json:"items"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

type NodeJSFoulAPI struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata"`
	Spec              NodeJSFoulAPISpec   `json:"spec"`
	Status            NodeJSFoulAPIStatus `json:"status,omitempty"`
}

type NodeJSFoulAPISpec struct {
	// Fill me
}
type NodeJSFoulAPIStatus struct {
	// Fill me
}
