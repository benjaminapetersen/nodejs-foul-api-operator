#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

vendor/k8s.io/code-generator/generate-groups.sh \
deepcopy \
github.com/benjaminapetersen/nodejs-foul-api-operator/pkg/generated \
github.com/benjaminapetersen/nodejs-foul-api-operator/pkg/apis \
nodejs-foul-api:v1alpha1 \
--go-header-file "./tmp/codegen/boilerplate.go.txt"
